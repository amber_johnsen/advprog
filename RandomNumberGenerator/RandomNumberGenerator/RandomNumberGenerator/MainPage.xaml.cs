﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RandomNumberGenerator
{
    public partial class RandomNumberGenerator : ContentPage
    {
        public static string WhereIsImage = "http://lorempixel.com/200/300/city/";

        public RandomNumberGenerator()
       {
            InitializeComponent();

            Random rnd = new Random();
            var r1 = rnd.Next(1, 10);

            var fullurl = $"{WhereIsImage} {r1}";
            //WhereIsImage is a string

            var urlLocation = new UriImageSource { Uri = new Uri(fullurl) };

            urlLocation.CachingEnabled = false;

            webimage.Source = urlLocation;  //webimage = x:Name
        }
    }
}
